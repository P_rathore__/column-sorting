/**
 * @description       : This class is Controller class for ClientSideColumnSorting LWC
 * @Created By :      : Poonam Rathore
 * @Created on        : 07-07-2021
 * @last modified by  : Poonam Rathore
 * Modifications Log 
 * Ver   Date         Author           Modification
 * 1.0   07-06-2021   Poonam Rathore   Initial Version
**/

public with sharing class ClientSideColumnSortingController {
    /**
    * @description This method is used to get all the account records
    * @author Poonam Rathore | 07-06-2021 
    * @return List<AccountWrapper> 
    **/
    @AuraEnabled
    public static List<AccountWrapper> getAccountRecords(){
        List<Account> accountList = [SELECT Id,Name,Industry,AnnualRevenue,Site,Type,LastModifiedDate FROM Account 
                                     WHERE Industry !=null AND AnnualRevenue !=null AND Site !=null AND Type !=null];
        List<AccountWrapper> warpperList = new List<AccountWrapper>();
        for(Account acc : accountList){
            AccountWrapper wrap = new AccountWrapper(acc);
            warpperList.add(wrap);
        }
        return warpperList;

    }
    public class AccountWrapper{
        @AuraEnabled public String id;
        @AuraEnabled public String name;
        @AuraEnabled public String industry;
        @AuraEnabled public Decimal annualRevenue;
        @AuraEnabled public String site;
        @AuraEnabled public String type;
        @AuraEnabled public Datetime lastModifiedDate; 

        public AccountWrapper(Account acc) {
            this.id = acc.Id;
            this.name = acc.Name;
            this.industry = acc.Industry;
            this.annualRevenue = acc.AnnualRevenue;
            this.site = acc.Site;
            this.type = acc.Type;
            this.lastModifiedDate = acc.LastModifiedDate;
        }
    }
}
