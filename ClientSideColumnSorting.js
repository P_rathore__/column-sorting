/**
 * Created By :      : Poonam Rathore
 * Created on        : 07-07-2021
 * last modified by  : Poonam Rathore
 * Modifications Log 
 * Ver   Date         Author           Modification
 * 1.0   07-07-2021   Poonam Rathore   Initial Version
**/
import { LightningElement,track } from 'lwc';
import getAccountRecords from '@salesforce/apex/ClientSideColumnSortingController.getAccountRecords';  

export default class ClientSideColumnSorting extends LightningElement {
    @track tableRecords=[];
    @track columns = [{
        name:'name',
        label:'Name',
        order:'DESC',
        arrowDown:false,
        arrowUp:false
    },{
        name:'industry',
        label:'Industry',
        order:'DESC',
        arrowDown:false,
        arrowUp:false
    },{
        name:'site',
        label:'Site',
        order:'DESC',
        arrowDown:false,
        arrowUp:false
    },{
        name:'annualRevenue',
        label:'Annual Revenue',
        order:'DESC',
        arrowDown:false,
        arrowUp:false
    },{
        name:'type',
        label:'Type',
        order:'DESC',
        arrowDown:false,
        arrowUp:false
    },{
        name:'lastModifiedDate',
        label:'Last Modified Date',
        order:'DESC',
        arrowDown:false,
        arrowUp:false
    }];
    connectedCallback(){
        this.getAccountRecords();
    }
    /*
    * This method is used to do an apex call for getting all account records 
    **/
    getAccountRecords(){
        getAccountRecords({})
        .then((result)=>{
            this.tableRecords= result;
        })
        .catch((error)=>{
            console.log('getBudgetLineRecords Error  : ' + JSON.stringify(error)); 
        })
        .finally(()=>{
            this.tableRecords.forEach((element,index)=>{
                element.sNo = index + 1;
            });
        });
    }
    /*
    * This method is used to perform sorting operation on column data
    **/
    handleSort(event){
        let order;
        let currentHeader = this.columns.filter(ele=>ele.name == event.currentTarget.dataset.name)[0];
        if(currentHeader.order=='DESC'){
            order = 1;
            currentHeader.order = 'ASC';
            currentHeader.arrowDown =true;
            currentHeader.arrowUp =false;
        }else {
            order = -1;
            currentHeader.order = 'DESC';
            currentHeader.arrowDown =false;
            currentHeader.arrowUp   =true;
        }
        this.tableRecords.sort((element1,element2)=>{
            // if type is string than by default JS sorts in case sensitive manner so in order to make it insensitive this check is added here 
            if(typeof element1[event.currentTarget.dataset.name] === 'string'){
                if(element1[event.currentTarget.dataset.name].toLowerCase() > element2[event.currentTarget.dataset.name].toLowerCase()){
                    return order *1;    
                }
                return order*-1;
            }
            if(element1[event.currentTarget.dataset.name] > element2[event.currentTarget.dataset.name]){
                return order *1;
            }
                return order*-1;
        });

        //Removing arrows from other columns 
        for(const index in this.columns){
            if(this.columns[index].name != currentHeader.name){
                this.columns[index].arrowDown = false;
                this.columns[index].arrowUp = false;
            }
        }
    }
}